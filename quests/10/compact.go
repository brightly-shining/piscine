package piscine

func Compact(ptr *[]string) int {
	strings := *ptr
	compactedStrings := []string{}
	for _, str := range strings {
		if str != "" {
			compactedStrings = append(compactedStrings, str)
		}
	}

	*ptr = compactedStrings
	return len(compactedStrings)
}
