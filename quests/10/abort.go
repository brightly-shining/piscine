package piscine

func Abort(a, b, c, d, e int) int {
	numbers := []int{a, b, c, d, e}
	Sort(numbers)
	return numbers[2]
}

func Sort(numbers []int) {
	for i := 0; i < len(numbers)-1; i++ {
		for i2 := i + 1; i2 < len(numbers); i2++ {
			if numbers[i2] < numbers[i] {
				numbers[i], numbers[i2] = numbers[i2], numbers[i]
			}
		}
	}
}
