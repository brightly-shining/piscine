package piscine

func Join(strs []string, sep string) string {
	length := len(strs)
	finalString := ""

	for index, str := range strs {
		finalString += str

		if index != length-1 {
			finalString += sep
		}
	}
	return finalString
}
