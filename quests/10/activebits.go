package piscine

func ActiveBits(n int) int {
	activeBits := 0

	for n > 0 {
		if n%2 == 1 {
			activeBits++
		}
		n /= 2
	}
	return activeBits
}
