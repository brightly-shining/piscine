package main

import (
	"fmt"
	"os"
)

func main() {
	arguments := os.Args[1:]

	for _, argument := range arguments {
		if argument == "01" || argument == "galaxy" || argument == "galaxy 01" {
			fmt.Println("Alert!!!")
			return
		}
	}
}
