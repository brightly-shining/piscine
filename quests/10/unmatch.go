package piscine

func Unmatch(a []int) int {
	var unpaired []int

	for _, num := range a {
		found := false
		for i, v := range unpaired {
			if v == num {
				unpaired = append(unpaired[:i], unpaired[i+1:]...)
				found = true
				break
			}
		}
		if !found {
			unpaired = append(unpaired, num)
		}
	}

	if len(unpaired) > 0 {
		return unpaired[0]
	}
	return -1
}
