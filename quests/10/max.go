package piscine

func Max(a []int) int {
	maxValue := 0

	for _, number := range a {
		if number > maxValue {
			maxValue = number
		}
	}
	return maxValue
}
