package piscine

func Rot14(s string) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ")

	var runes []rune
outer:
	for _, char := range s {
		for index, letter := range letters {
			if char == letter {
				newIndex := index + 14
				if newIndex >= len(letters) {
					newIndex = newIndex - len(letters) - 1
				}
				runes = append(runes, letters[newIndex])
				continue outer
			}
		}
		runes = append(runes, char)
	}
	return string(runes)
}
