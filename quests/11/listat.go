package piscine

func ListAt(node *NodeL, pos int) *NodeL {
	if pos < 0 {
		return nil
	}

	currentPos := 0
	headNode := node
	for {
		if headNode == nil {
			return nil
		} else if currentPos == pos {
			return headNode
		}

		headNode = headNode.Next
		currentPos++
	}
}
