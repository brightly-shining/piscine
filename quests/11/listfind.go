package piscine

func CompStr(a, b interface{}) bool {
	return a == b
}

func ListFind(list *List, ref interface{}, comp func(a, b interface{}) bool) *interface{} {
	headNode := list.Head
	for headNode != nil {
		if comp(headNode.Data, ref) {
			return &headNode.Data
		}
		headNode = headNode.Next
	}
	return nil
}
