package piscine

func SortedListMerge(n1 *NodeI, n2 *NodeI) *NodeI {
	if n1 == nil {
		return n2
	}

	if n1.Next == nil {
		n1.Next = n2
		return n1
	}

	lastNode := n1
	for {
		nextNode := lastNode.Next
		if nextNode == nil {
			break
		}
		lastNode = nextNode
	}

	lastNode.Next = n2
	return ListSort(n1)
}
