package piscine

type NodeI struct {
	Data int
	Next *NodeI
}

func ListSort(headNode *NodeI) *NodeI {
	nodes := []NodeI{}

	if headNode == nil || headNode.Next == nil {
		return headNode
	}

	for node := headNode; node != nil; node = node.Next {
		nodes = append(nodes, *node)
	}
	Sort(nodes)

	nodes[len(nodes)-1].Next = nil
	headNode = &nodes[0]
	for i := 1; i < len(nodes); i++ {
		headNode.Next = &nodes[i]
		headNode = &nodes[i]
	}

	return &nodes[0]
}

func Sort(nodes []NodeI) {
	for i := 0; i < len(nodes)-1; i++ {
		for i2 := i + 1; i2 < len(nodes); i2++ {
			if nodes[i2].Data < nodes[i].Data {
				nodes[i], nodes[i2] = nodes[i2], nodes[i]
			}
		}
	}
}
