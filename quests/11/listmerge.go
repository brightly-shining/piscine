package piscine

func ListMerge(firstList *List, secondList *List) {
	if firstList.Head == nil {
		firstList.Head = secondList.Head
		return
	}

	lastNode := firstList.Head
	for lastNode.Next != nil {
		lastNode = lastNode.Next
	}
	lastNode.Next = secondList.Head
}
