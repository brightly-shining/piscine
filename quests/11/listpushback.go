package piscine

type NodeL struct {
	Data interface{}
	Next *NodeL
}

type List struct {
	Head *NodeL
	Tail *NodeL
}

func ListPushBack(list *List, data interface{}) {
	nodeL := NodeL{
		Data: data,
		Next: nil,
	}

	if list.Head == nil {
		list.Head = &nodeL
		return
	}

	lastNode := list.Head
	for {
		nextNode := lastNode.Next

		if nextNode == nil {
			lastNode.Next = &nodeL
			break
		}

		lastNode = nextNode
	}
}
