package piscine

func ListReverse(list *List) {
	nodes := []NodeL{}

	headNode := list.Head
	for {
		if headNode == nil {
			break
		}
		nodes = append([]NodeL{*headNode}, nodes...)
		headNode = headNode.Next
	}

	if len(nodes) <= 1 {
		return
	}

	nodes[len(nodes)-1].Next = nil
	list.Head = &nodes[0]
	currentNode := list.Head

	for i := 1; i < len(nodes); i++ {
		currentNode.Next = &nodes[i]
		currentNode = &nodes[i]
	}
}
