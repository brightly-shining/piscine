package piscine

func ListRemoveIf(list *List, data_ref interface{}) {
	nodes := []NodeL{}

	for headNode := list.Head; headNode != nil; headNode = headNode.Next {
		if headNode.Data != data_ref {
			nodes = append(nodes, *headNode)
		}
	}

	if len(nodes) == 0 {
		list.Head = nil
		return
	}

	if len(nodes) == 1 {
		list.Head = &nodes[0]
		return
	}

	nodes[len(nodes)-1].Next = nil
	headNode := &nodes[0]
	list.Head = headNode
	for i := 1; i < len(nodes); i++ {
		headNode.Next = &nodes[i]
		headNode = &nodes[i]
	}
}
