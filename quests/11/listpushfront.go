package piscine

func ListPushFront(list *List, data interface{}) {
	nodeL := NodeL{
		Data: data,
		Next: nil,
	}

	if list.Head == nil {
		list.Head = &nodeL
		return
	}

	nodeL.Next = list.Head
	list.Head = &nodeL
}
