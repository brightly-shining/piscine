package piscine

func IsPositiveNode(node *NodeL) bool {
	switch node.Data.(type) {
	case int, float32, float64, byte:
		return node.Data.(int) > 0
	default:
		return false
	}
}

func IsAlNode(node *NodeL) bool {
	switch node.Data.(type) {
	case int, float32, float64, byte:
		return false
	default:
		return true
	}
}

func ListForEachIf(list *List, f func(*NodeL), cond func(*NodeL) bool) {
	headNode := list.Head
	for headNode != nil {
		if cond(headNode) {
			f(headNode)
		}
		headNode = headNode.Next
	}
}
