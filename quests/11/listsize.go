package piscine

func ListSize(list *List) int {
	if list.Head == nil {
		return 0
	}

	lastNode := list.Head
	counter := 1

	for {
		nextNode := lastNode.Next
		if nextNode == nil {
			return counter
		}
		lastNode = nextNode
		counter++
	}
}
