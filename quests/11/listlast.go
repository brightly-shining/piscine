package piscine

func ListLast(list *List) interface{} {
	if list.Head == nil {
		return nil
	}

	headNode := list.Head
	for {
		nextNode := headNode.Next
		if nextNode == nil {
			break
		}
		headNode = nextNode
	}
	return headNode.Data
}
