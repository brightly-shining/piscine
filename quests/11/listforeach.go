package piscine

func Add2_node(node *NodeL) {
	switch node.Data.(type) {
	case int:
		node.Data = node.Data.(int) + 2
	case string:
		node.Data = node.Data.(string) + "2"
	}
}

func Subtract3_node(node *NodeL) {
	switch node.Data.(type) {
	case int:
		node.Data = node.Data.(int) - 3
	case string:
		node.Data = node.Data.(string) + "-3"
	}
}

func ListForEach(list *List, f func(*NodeL)) {
	headNode := list.Head
	for {
		if headNode == nil {
			return
		}
		f(headNode)
		headNode = headNode.Next
	}
}
