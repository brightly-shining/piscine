package piscine

func SortWordArr(str []string) {
	for i := 0; i < len(str)-1; i++ {
		for i2 := i + 1; i2 < len(str); i2++ {
			if str[i2] < str[i] {
				str[i], str[i2] = str[i2], str[i]
			}
		}
	}
}
