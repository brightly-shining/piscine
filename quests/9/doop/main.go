package main

import (
	"os"
)

func ConvertNumber(n int) (string, bool) {
	buf := [11]byte{}
	pos := len(buf)
	i := int64(n)
	signed := i < 0
	if signed {
		i = -i
	}
	for {
		pos--
		if pos >= len(buf) || pos < 0 {
			return "", true
		}
		buf[pos], i = '0'+byte(i%10), i/10
		if i == 0 {
			if signed {
				pos--
				buf[pos] = '-'
			}
			return string(buf[pos:]), false
		}
	}
}

func ConvertString(num string) (int, bool) {
	isNegative := false
	if rune(num[0]) == '-' {
		isNegative = true
		num = num[1:]
	}
	number := 0
	for _, char := range num {
		digit := int(char) - 48
		if digit > 9 || digit < 0 {
			return 0, true
		}
		number = number*10 + digit
	}
	if isNegative {
		return -number, false
	}
	return number, false
}

func Print(str string) {
	os.Stdout.Write([]byte(str + "\n"))
}

func PrintNumber(numb int) {
	num, err := ConvertNumber(numb)
	if err {
		return
	}
	Print(num)
}

func HandleOperation(fNum string, operator string, sNum string) {
	fNumber, err := ConvertString(fNum)
	if err {
		return
	}
	sNumber, err := ConvertString(sNum)
	if err {
		return
	}

	switch operator {
	case "+":
		PrintNumber(fNumber + sNumber)
	case "-":
		PrintNumber(fNumber - sNumber)
	case "*":
		PrintNumber(fNumber * sNumber)
	case "%":
		if sNumber == 0 {
			Print("No modulo by 0")
		} else {
			PrintNumber(fNumber % sNumber)
		}
	case "/":
		if sNumber == 0 {
			Print("No division by 0")
		} else {
			PrintNumber(fNumber / sNumber)
		}
	}
}

func main() {
	args := os.Args[1:]

	if len(args) != 3 {
		return
	}

	HandleOperation(args[0], args[1], args[2])
}
