package piscine

func ForEach(f func(int), initialNumbers []int) {
	for _, numb := range initialNumbers {
		f(numb)
	}
}
