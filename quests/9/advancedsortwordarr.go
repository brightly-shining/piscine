package piscine

func AdvancedSortWordArr(a []string, f func(a, b string) int) {
	for i := 0; i < len(a)-1; i++ {
		for i2 := i + 1; i2 < len(a); i2++ {
			result := f(a[i2], a[i])

			if result < 1 {
				a[i], a[i2] = a[i2], a[i]
			}
		}
	}
}
