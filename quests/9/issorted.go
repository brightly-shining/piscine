package piscine

func IsSorted(f func(a, b int) int, a []int) bool {
	ascending, descending := true, true

	for i := 1; i < len(a); i++ {
		cmp := f(a[i-1], a[i])
		if cmp > 0 {
			ascending = false
		}
		if cmp < 0 {
			descending = false
		}
	}

	return ascending || descending
}
