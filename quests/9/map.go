package piscine

func Map(f func(int) bool, a []int) []bool {
	var results []bool
	for _, numb := range a {
		results = append(results, f(numb))
	}
	return results
}
