package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	runes := []rune(os.Args[0])
	length := len(runes)
	name := []rune{}
	for i := length - 1; i > 0; i-- {
		if runes[i] == '/' {
			break
		}
		name = append([]rune{runes[i]}, name...)
	}
	for _, char := range name {
		z01.PrintRune(char)
	}
	z01.PrintRune('\n')
}
