package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	arguments := os.Args[1:]

	if len(arguments) < 1 {
		z01.PrintRune('\n')
		return
	}

	string := reverseVowels(joinArguments(arguments, " "))

	for _, char := range string {
		z01.PrintRune(char)
	}
	z01.PrintRune('\n')
}

func reverseVowels(s string) string {
	runes := []rune(s)

	vowelPositions := []int{}
	for i, char := range runes {
		if isVowel(char) {
			vowelPositions = append(vowelPositions, i)
		}
	}

	for i := 0; i < len(vowelPositions)/2; i++ {
		runes[vowelPositions[i]], runes[vowelPositions[len(vowelPositions)-1-i]] = runes[vowelPositions[len(vowelPositions)-1-i]], runes[vowelPositions[i]]
	}

	return string(runes)
}

func joinArguments(arguments []string, separator string) string {
	resultString := ""
	for i := 0; i < len(arguments)-1; i++ {
		resultString += arguments[i] + separator
	}
	resultString += arguments[len(arguments)-1]

	return resultString
}

func isVowel(char rune) bool {
	for _, vowel := range "AEIOUaeiou" {
		if char == vowel {
			return true
		}
	}
	return false
}
