package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	parameters := []string{}

	for index, str := range os.Args {
		if index == 0 {
			continue
		}
		parameters = append([]string{str}, parameters...)
	}

	for _, str := range parameters {
		for _, char := range str {
			z01.PrintRune(char)
		}
		z01.PrintRune('\n')
	}
}
