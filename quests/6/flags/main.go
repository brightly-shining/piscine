package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args[1:]

	if len(args) == 0 || containsHelpFlag(args) {
		printUsage()
		return
	}

	argString := args[len(args)-1]

	insertFlag, insertValue := findInsertFlag(args)
	orderFlag := containsFlag(args, "--order", "-o")

	if insertFlag {
		argString = insertString(argString, insertValue)
	}

	if orderFlag {
		argString = orderString(argString)
	}

	fmt.Println(argString)
}

func containsHelpFlag(args []string) bool {
	for _, arg := range args {
		if arg == "--help" || arg == "-h" {
			return true
		}
	}
	return false
}

func containsFlag(args []string, longFlag string, shortFlag string) bool {
	for _, arg := range args {
		if arg == longFlag || arg == shortFlag {
			return true
		}
	}
	return false
}

func findInsertFlag(args []string) (bool, string) {
	for _, arg := range args {
		if hasPrefix(arg, "--insert=") {
			insertValue := trimPrefix(arg, "--insert=")
			return true, insertValue
		} else if hasPrefix(arg, "-i=") {
			insertValue := trimPrefix(arg, "-i=")
			return true, insertValue
		}
	}
	return false, ""
}

func hasPrefix(s, prefix string) bool {
	if len(s) < len(prefix) {
		return false
	}
	for i := 0; i < len(prefix); i++ {
		if s[i] != prefix[i] {
			return false
		}
	}
	return true
}

func trimPrefix(s, prefix string) string {
	if hasPrefix(s, prefix) {
		return s[len(prefix):]
	}
	return s
}

func insertString(original, insert string) string {
	parts := splitAfter(original, insert)
	return parts[0] + insert + parts[1]
}

func splitAfter(s, substr string) []string {
	index := len(s)
	if i := indexOfSubstring(s, substr); i != -1 {
		index = i + len(substr)
	}
	return []string{s[:index], s[index:]}
}

func indexOfSubstring(s, substr string) int {
	n := len(s)
	m := len(substr)
	for i := 0; i < n-m+1; i++ {
		if s[i:i+m] == substr {
			return i
		}
	}
	return -1
}

func orderString(s string) string {
	slice := []byte(s)
	for i := 0; i < len(slice)-1; i++ {
		for j := 0; j < len(slice)-i-1; j++ {
			if slice[j] > slice[j+1] {
				slice[j], slice[j+1] = slice[j+1], slice[j]
			}
		}
	}
	return string(slice)
}

func printUsage() {
	fmt.Println("--insert")
	fmt.Println("  -i")
	fmt.Println("\t This flag inserts the string into the string passed as argument.")
	fmt.Println("--order")
	fmt.Println("  -o")
	fmt.Println("\t This flag will behave like a boolean, if it is called it will order the argument.")
}
