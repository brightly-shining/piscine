package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	for index, str := range os.Args {
		if index == 0 {
			continue
		}

		for _, char := range str {
			z01.PrintRune(char)
		}
		z01.PrintRune('\n')
	}
}
