package main

import (
	"os"

	"github.com/01-edu/z01"
)

func PrintStr(str string) {
	for _, char := range str {
		z01.PrintRune(char)
	}
	z01.PrintRune('\n')
}

func CopySlice(slice []string) []string {
	var sliceCopy []string
	sliceCopy = append(sliceCopy, slice...)
	return sliceCopy
}

func RemoveFromSlice(slice []string, index int) []string {
	slice[index] = slice[len(slice)-1]
	slice[len(slice)-1] = ""
	return slice[:len(slice)-1]
}

func Sort(initialSlice []string) []string {
	slice := CopySlice(initialSlice)
	var sortedSlice []string
	for range slice {
		indexToRemove := 0
		for index := range slice {
			if rune(slice[indexToRemove][0]) > rune(slice[index][0]) {
				indexToRemove = index
			}
		}
		sortedSlice = append(sortedSlice, slice[indexToRemove])
		slice = RemoveFromSlice(slice, indexToRemove)
	}
	return sortedSlice
}

func main() {
	reordered := Sort(os.Args[1:])
	for _, str := range reordered {
		PrintStr(str)
	}
}
