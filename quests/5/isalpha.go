package piscine

func IsAlpha(s string) bool {
	for _, char := range s {
		IsAlpha := false
		if char <= 90 && char >= 65 {
			IsAlpha = true
		} else if char <= 122 && char >= 97 {
			IsAlpha = true
		} else if char <= 57 && char >= 48 {
			IsAlpha = true
		}

		if !IsAlpha {
			return false
		}
	}
	return true
}
