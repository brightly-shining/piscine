package piscine

func InplaceReverse(slice []rune) []rune {
	for i, j := 0, len(slice)-1; i < j; i, j = i+1, j-1 {
		slice[i], slice[j] = slice[j], slice[i]
	}
	return slice
}

func GetNumbers(runes []rune) []int {
	var numbers []int
	for _, rune := range runes {
		if rune >= 48 && rune <= 57 {
			number := RuneToInt(rune)
			numbers = append(numbers, number)
		}
	}
	return numbers
}

func RuneToInt(convertableRune rune) int {
	convertableRune -= 48
	switch convertableRune {
	case 1:
		return 1
	case 2:
		return 2
	case 3:
		return 3
	case 4:
		return 4
	case 5:
		return 5
	case 6:
		return 6
	case 7:
		return 7
	case 8:
		return 8
	case 9:
		return 9
	default:
		return 0
	}
}

func TrimAtoi(str string) int {
	var isNegative bool
	for _, rune := range str {
		if rune >= 48 && rune <= 57 {
			break
		} else if rune == '-' {
			isNegative = true
			break
		}
	}

	numbers := GetNumbers(InplaceReverse([]rune(str)))
	number := 0
	for index := len(numbers) - 1; index >= 0; index-- {
		addend := numbers[index]
		for i := index; i > 0; i-- {
			addend *= 10
		}
		number += addend
	}

	if isNegative {
		return -number
	} else {
		return number
	}
}
