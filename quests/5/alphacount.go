package piscine

func AlphaCount(s string) int {
	runes := []rune(s)
	counter := 0
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	for _, char := range runes {
		for _, letter := range letters {
			if char == letter {
				counter++
				break
			}
		}
	}
	return counter
}
