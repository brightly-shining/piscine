package piscine

func IsLower(s string) bool {
	for _, char := range s {
		if !(char <= 122 && char >= 97) {
			return false
		}
	}
	return true
}
