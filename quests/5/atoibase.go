package piscine

func AtoiBase(s, base string) int {
	if (len(base) < 2) || ContainsSign(base) || HasDuplicates(base) {
		return 0
	}
	return convertToBase(s, base)
}

func convertToBase(str, base string) int {
	baseMap := make(map[rune]int)

	for i, digit := range base {
		baseMap[digit] = i
	}

	result := 0
	baseLen := len(base)

	for i, char := range str {
		exponent := len(str) - i - 1
		result += baseMap[char] * pow(baseLen, exponent)
	}

	return result
}

func pow(x, n int) int {
	result := 1
	for n > 0 {
		if n%2 == 1 {
			result *= x
		}
		x *= x
		n /= 2
	}
	return result
}

func HasDuplicates(base string) bool {
	set := make(map[rune]bool)
	for _, char := range base {
		set[char] = true
	}

	return !(len(set) == len(base))
}

func ContainsSign(base string) bool {
	for _, char := range base {
		if char == '-' || char == '+' {
			return true
		}
	}
	return false
}
