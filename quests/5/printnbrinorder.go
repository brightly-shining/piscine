package piscine

import (
	"github.com/01-edu/z01"
)

func InplaceBubbleSort(numbers []int) []int {
	for i := 0; i < len(numbers); i++ {
		for index := 0; index < len(numbers); index++ {
			if index+1 == len(numbers) {
				break
			}
			if numbers[index] > numbers[index+1] {
				this := numbers[index]
				numbers[index] = numbers[index+1]
				numbers[index+1] = this
			}
		}
	}
	return numbers
}

func PrintNumbers(numbers []int) {
	for _, number := range numbers {
		z01.PrintRune(rune(48 + number))
	}
}

func PrintNbrInOrder(number int) {
	if number == 0 {
		PrintNumbers([]int{0})
	}

	var numbers []int
	for number > 0 {
		remainder := number % 10
		numbers = append(numbers, remainder)
		number -= remainder
		number /= 10
	}
	InplaceBubbleSort(numbers)
	PrintNumbers(numbers)
}
