package piscine

func Upper(letter rune) rune {
	uppercaseLetters := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	lowercaseLetters := []rune("abcdefghijklmnopqrstuvwxyz")
	for index, lowercasedLetter := range lowercaseLetters {
		if lowercasedLetter == letter {
			return uppercaseLetters[index]
		}
	}
	return letter
}

func LowerString(str string) string {
	uppercaseLetters := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	lowercaseLetters := []rune("abcdefghijklmnopqrstuvwxyz")
	var runes []rune
outer:
	for _, letterRune := range str {
		for index, uppercaseLetterRune := range uppercaseLetters {
			if letterRune == uppercaseLetterRune {
				runes = append(runes, lowercaseLetters[index])
				continue outer
			}
		}
		runes = append(runes, letterRune)
	}
	return string(runes)
}

func Capitalize(str string) string {
	var runes []rune
	var prevRune rune
	for index, letterRune := range []rune(LowerString(str)) {
		if index == 0 {
			runes = append(runes, Upper(letterRune))
			prevRune = letterRune
			continue
		}
		if prevRune == '+' || prevRune == ' ' || prevRune == '^' || prevRune == '$' || prevRune == '!' || prevRune == '@' || prevRune == '#' || prevRune == '%' || prevRune == '&' || prevRune == '*' || prevRune == '(' || prevRune == ')' || prevRune == '_' || prevRune == '=' || prevRune == '\'' || prevRune == '`' || prevRune == '"' || prevRune == '~' || prevRune == '/' || prevRune == '|' || prevRune == '\\' || prevRune == ':' || prevRune == ',' || prevRune == ';' || prevRune == '?' || prevRune == '>' || prevRune == '<' || prevRune == '[' || prevRune == ']' || prevRune == '{' || prevRune == '}' {
			runes = append(runes, Upper(letterRune))
			prevRune = letterRune
			continue
		}
		runes = append(runes, letterRune)
		prevRune = letterRune
	}
	return string(runes)
}
