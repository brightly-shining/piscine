package piscine

func ToLower(s string) string {
	lowercase := []rune("abcdefghijklmnopqrstuvwxyz")
	uppercase := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	str := ""

outer:
	for _, char := range s {
		for i, lChar := range uppercase {
			if char == lChar {
				str += string(lowercase[i])
				continue outer
			}
		}
		str += string(char)
	}
	return str
}
