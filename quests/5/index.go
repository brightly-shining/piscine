package piscine

func Index(s string, toFind string) int {
	stringRunes := []rune(s)
	runesToFind := []rune(toFind)

	if len(stringRunes) < len(runesToFind) {
		return -1
	} else if len(runesToFind) == 0 {
		return 0
	}

outer:
	for index, char := range stringRunes {
		if len(stringRunes)-index < len(runesToFind) {
			return -1
		}
		if char != runesToFind[0] {
			continue
		}

		for i, pChar := range runesToFind {
			if pChar != stringRunes[index+i] {
				continue outer
			}
		}
		return index
	}
	return -1
}
