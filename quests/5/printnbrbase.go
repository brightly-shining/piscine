package piscine

import (
	"github.com/01-edu/z01"
)

func PrintNbrBase(nbr int, base string) {
	if (len(base) < 2) || containsSign(base) || hasDuplicates(base) {
		printString("NV")
		return
	}
	if nbr == -9223372036854775808 {
		printString("-9223372036854775808")
		return
	}

	printNumberInBase(nbr, base)
}

func printNumberInBase(nbr int, base string) {
	baseLength := len(base)
	if nbr < 0 {
		z01.PrintRune('-')
		nbr = -nbr
	}

	if nbr >= baseLength {
		printNumberInBase(nbr/baseLength, base)
	}
	if nbr > 0 || (nbr == 0 && len(base) == 1) {
		z01.PrintRune(rune(base[nbr%baseLength]))
	}
}

func printString(str string) {
	for _, char := range str {
		z01.PrintRune(char)
	}
}

func hasDuplicates(base string) bool {
	set := make(map[rune]bool)
	for _, char := range base {
		set[char] = true
	}

	return !(len(set) == len(base))
}

func containsSign(base string) bool {
	for _, char := range base {
		if char == '-' || char == '+' {
			return true
		}
	}
	return false
}
