package piscine

func ToUpper(s string) string {
	lowercase := []rune("abcdefghijklmnopqrstuvwxyz")
	uppercase := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	str := ""

outer:
	for _, char := range s {
		for i, lChar := range lowercase {
			if char == lChar {
				str += string(uppercase[i])
				continue outer
			}
		}
		str += string(char)
	}
	return str
}
