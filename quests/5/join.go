package piscine

func Join(strs []string, sep string) string {
	concatenated := ""
	for _, str := range strs {
		concatenated += sep
		concatenated += str
	}
	return concatenated[len(sep):]
}
