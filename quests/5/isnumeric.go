package piscine

func IsNumeric(s string) bool {
	for _, char := range s {
		if !(char <= 57 && char >= 48) {
			return false
		}
	}
	return true
}
