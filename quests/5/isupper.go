package piscine

func IsUpper(s string) bool {
	for _, char := range s {
		if !(char <= 90 && char >= 65) {
			return false
		}
	}
	return true
}
