package piscine

func BasicJoin(elems []string) string {
	concatenated := ""
	for _, elem := range elems {
		concatenated += elem
	}
	return concatenated
}
