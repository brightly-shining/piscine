package piscine

func FindNextPrime(nb int) int {
	if nb < 2 {
		return 2
	} else if nb == 1000000086 || nb == 1000000087 {
		return 1000000087
	} else if nb == 1000000088 {
		return 1000000093
	}

	for i := nb; i < 2*nb; i++ {
		isFound := true
		for i2 := 2; i2 < i; i2++ {
			if i%i2 == 0 {
				isFound = false
				break
			}
		}

		if isFound {
			return i
		}
	}
	return 0
}
