package piscine

func IterativePower(nb int, power int) int {
	if power == 0 {
		return 1
	} else if power < 0 {
		return 0
	}

	multiplier := nb
	for i := 1; i < power; i++ {
		nb *= multiplier
	}
	return nb
}
