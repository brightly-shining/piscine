package piscine

import "github.com/01-edu/z01"

const N = 8

func EightQueens() {
	board := make([]int, N)
	solve(0, board)
}

func solve(row int, board []int) {
	if row == N {
		printSolution(board)
		return
	}

	for col := 0; col < N; col++ {
		if isSafe(board, row, col) {
			board[row] = col
			solve(row+1, board)
		}
	}
}

func isSafe(board []int, row, col int) bool {
	for i := 0; i < row; i++ {
		if board[i] == col ||
			board[i]-i == col-row ||
			board[i]+i == col+row {
			return false
		}
	}
	return true
}

func printSolution(board []int) {
	for i := 0; i < N; i++ {
		z01.PrintRune(rune(board[i] + '0' + 1))
	}
	z01.PrintRune('\n')
}
