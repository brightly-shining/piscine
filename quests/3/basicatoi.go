package piscine

func BasicAtoi(s string) int {
	addends := []int{}
	number := 0
	for _, rune := range s {
		addends = append([]int{int(rune - '0')}, addends...)
	}

	for index, num := range addends {
		if index == 0 {
			number += num
			continue
		}
		multiplier := 10
		for i := 1; i < index; i++ {
			multiplier *= 10
		}
		number += num * multiplier
	}
	return number
}
