package piscine

func SortIntegerTable(table []int) {
	for range table {
		for i := 1; i < len(table); i++ {
			if table[i] < table[i-1] {
				table[i], table[i-1] = table[i-1], table[i]
			}
		}
	}
}
