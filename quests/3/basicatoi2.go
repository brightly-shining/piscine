package piscine

func BasicAtoi2(s string) int {
	addends := []int{}
	number := 0
	for _, rune := range s {
		convertedRune := int(rune - '0')
		if convertedRune < 0 || convertedRune > 9 {
			return 0
		}
		addends = append([]int{convertedRune}, addends...)
	}

	for index, num := range addends {
		if index == 0 {
			number += num
			continue
		}
		multiplier := 10
		for i := 1; i < index; i++ {
			multiplier *= 10
		}
		number += num * multiplier
	}
	return number
}
