package piscine

func Atoi(s string) int {
	addends := []int{}
	number := 0
	isPositive := true

	for index, rune := range s {
		convertedRune := int(rune - '0')
		if index == 0 {
			if rune == '-' {
				isPositive = false
				continue
			} else if rune == '+' {
				continue
			}
		}

		if convertedRune < 0 || convertedRune > 9 {
			return 0
		}
		addends = append([]int{convertedRune}, addends...)
	}

	for index, num := range addends {
		if index == 0 {
			number += num
			continue
		}
		multiplier := 10
		for i := 1; i < index; i++ {
			multiplier *= 10
		}
		number += num * multiplier
	}
	if !isPositive {
		number = -number
	}
	return number
}
