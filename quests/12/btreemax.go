package piscine

func BTreeMax(root *TreeNode) *TreeNode {
	rightNode := root
	for rightNode.Right != nil {
		rightNode = rightNode.Right
	}
	return rightNode
}
