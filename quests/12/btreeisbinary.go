package piscine

func BTreeIsBinary(root *TreeNode) bool {
	var prevValue string
	var isValid bool = true

	var inOrderTraversal func(node *TreeNode)
	inOrderTraversal = func(node *TreeNode) {
		if node == nil || !isValid {
			return
		}

		inOrderTraversal(node.Left)

		if prevValue != "" && node.Data <= prevValue {
			isValid = false
			return
		}
		prevValue = node.Data

		inOrderTraversal(node.Right)
	}

	inOrderTraversal(root)

	return isValid
}
