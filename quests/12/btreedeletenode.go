package piscine

func BTreeDeleteNode(root, node *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	if root.Data == node.Data {
		if root.Left == nil && root.Right == nil {
			return nil
		} else if root.Left == nil && root.Right != nil {
			return root.Right
		} else if root.Right == nil && root.Left != nil {
			return root.Left
		}

		root.Data = BTreeMin(root.Right).Data
		root.Right = BTreeDeleteNode(root.Right, BTreeMin(root.Right))
	}

	if root.Data > node.Data {
		root.Left = BTreeDeleteNode(root.Left, node)
	} else if root.Data < node.Data {
		root.Right = BTreeDeleteNode(root.Right, node)
	}
	return root
}
