package piscine

func BTreeMin(root *TreeNode) *TreeNode {
	leftNode := root
	for leftNode.Left != nil {
		leftNode = leftNode.Left
	}
	return leftNode
}
