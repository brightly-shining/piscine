package piscine

import "github.com/01-edu/z01"

func Combinations(elements string, k int) []string {
	if k == 0 {
		return []string{""}
	}
	if len(elements) == 0 || k > len(elements) {
		return []string{}
	}

	first := elements[0]
	rest := elements[1:]

	combsWithoutFirst := Combinations(rest, k)
	combsWithFirst := Combinations(rest, k-1)

	var result []string
	for _, combo := range combsWithFirst {
		result = append(result, string(first)+combo)
	}
	result = append(result, combsWithoutFirst...)
	return result
}

func Print(combinationList []string) {
	lastItemIndex := len(combinationList) - 1

	for index, str := range combinationList {
		for _, char := range str {
			z01.PrintRune(char)
		}

		if index < lastItemIndex {
			z01.PrintRune(',')
			z01.PrintRune(' ')
		}
	}
	z01.PrintRune('\n')
}

func PrintCombN(n int) {
	Print(Combinations("0123456789", n))
}
