package main

import "github.com/01-edu/z01"

func main() {
	for _, char := range "abcdefghijklmnopqrstuvwxyz\n" {
		z01.PrintRune(char)
	}
}
