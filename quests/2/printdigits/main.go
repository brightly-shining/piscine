package main

import "github.com/01-edu/z01"

func main() {
	for _, char := range "0123456789\n" {
		z01.PrintRune(char)
	}
}
