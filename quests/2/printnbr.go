package piscine

import "github.com/01-edu/z01"

func transcript(number int) rune {
	if number < 0 {
		number = -number
	}

	switch number {
	case 1:
		return '1'
	case 2:
		return '2'
	case 3:
		return '3'
	case 4:
		return '4'
	case 5:
		return '5'
	case 6:
		return '6'
	case 7:
		return '7'
	case 8:
		return '8'
	case 9:
		return '9'
	default:
		return '0'
	}
}

func PrintNbr(n int) {
	isNumberPositive := true
	if n < 0 {
		isNumberPositive = false
	}

	runes := []rune{}
	for {
		remainder := n % 10
		runes = append([]rune{transcript(remainder)}, runes...)
		n -= remainder

		if n == 0 {
			break
		}
		n /= 10
	}

	if !isNumberPositive {
		z01.PrintRune('-')
	}
	for _, char := range runes {
		z01.PrintRune(char)
	}
}
