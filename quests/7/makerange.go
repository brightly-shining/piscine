package piscine

func MakeRange(min, max int) []int {
	if min >= max {
		return nil
	}

	length := max - min
	slice := make([]int, length)

	index := 0
	for i := min; i < max; i++ {
		slice[index] = i
		index++
	}
	return slice
}
