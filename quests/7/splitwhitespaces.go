package piscine

func SplitWhiteSpaces(s string) []string {
	slice := []string{}
	maxIndex := len([]rune(s)) - 1

	currentWord := ""
	for index, char := range s {
		if char == ' ' {
			if currentWord != "" {
				slice = append(slice, currentWord)
			}
			currentWord = ""
			continue
		}
		if index == maxIndex {
			currentWord += string(char)
			slice = append(slice, currentWord)
			continue
		}
		currentWord += string(char)
	}
	return slice
}
