package piscine

func ConvertBase(nbr, baseFrom, baseTo string) string {
	decimalValue := convertToDecimal(nbr, baseFrom)
	return convertFromDecimal(decimalValue, baseTo)
}

func convertToDecimal(nbr, baseFrom string) int {
	baseFromValue := len(baseFrom)
	decimalValue := 0

	for _, digit := range nbr {
		digitValue := indexOfRune(baseFrom, digit)
		decimalValue = decimalValue*baseFromValue + digitValue
	}

	return decimalValue
}

func convertFromDecimal(decimalValue int, baseTo string) string {
	baseToValue := len(baseTo)
	result := ""

	if decimalValue == 0 {
		return string(baseTo[0])
	}

	for decimalValue > 0 {
		remainder := decimalValue % baseToValue
		result = string(runeAtIndex(baseTo, remainder)) + result
		decimalValue /= baseToValue
	}

	return result
}

func indexOfRune(s string, r rune) int {
	for i, char := range s {
		if char == r {
			return i
		}
	}
	return -1
}

func runeAtIndex(s string, index int) rune {
	if index >= 0 && index < len(s) {
		return []rune(s)[index]
	}
	return 0
}
