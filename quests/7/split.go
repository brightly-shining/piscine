package piscine

func Split(str, sep string) []string {
	var strSlice []string
	var strPart string
outer:
	for index := 0; index < len(str); index++ {
		char := str[index]
		if char != sep[0] {
			strPart += string(char)
		} else {
			for i := 1; i < len(sep); i++ {
				if str[index+i] != sep[i] {
					strPart += string(char)
					continue outer
				}
			}
			strSlice = append(strSlice, strPart)
			strPart = ""
			index += len(sep) - 1
		}
	}

	if strPart != "" {
		strSlice = append(strSlice, strPart)
	}
	return strSlice
}
