package piscine

func ConcatParams(args []string) string {
	result := ""
	for index, str := range args {
		if index == 0 {
			result += str
			continue
		}
		result = result + "\n" + str
	}
	return result
}
