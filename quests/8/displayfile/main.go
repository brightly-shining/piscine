package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func printFileContent(path string) {
	content, _ := ioutil.ReadFile(path)
	fmt.Print(string(content))
}

func main() {
	arguments := os.Args
	length := len(arguments)

	if length == 1 {
		fmt.Println("File name missing")
	} else if length == 2 {
		printFileContent(arguments[1])
	} else {
		fmt.Println("Too many arguments")
	}
}
