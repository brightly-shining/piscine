package main

import (
	"io"
	"os"

	"github.com/01-edu/z01"
)

func PrintString(str string) {
	for _, char := range str {
		z01.PrintRune(char)
	}
}

func GetFileContent(filepath string) string {
	data, err := os.ReadFile(filepath)
	if err != nil {
		PrintString("ERROR: " + "open " + filepath + ": no such file or directory\n")
		os.Exit(1)
	}
	return string(data)
}

func main() {
	arguments := os.Args[1:]

	if len(arguments) == 0 {
		io.Copy(os.Stdout, os.Stdin)
	} else {
		for _, filepath := range arguments {
			PrintString(GetFileContent(filepath))
		}
	}
}
