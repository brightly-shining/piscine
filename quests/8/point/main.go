package main

import (
	"github.com/01-edu/z01"
)

type point struct {
	x int
	y int
}

func setPoint(ptr *point) {
	ptr.x = 42
	ptr.y = 21
}

func main() {
	points := &point{42, 21}
	setPoint(points)
	for _, char := range "x = 42, y = 21\n" {
		z01.PrintRune(char)
	}
}
